import React from 'react';
import { useStyle } from '@magento/venia-ui/lib/classify';
import { shape, string } from 'prop-types';

import defaultClasses from './feature.css';
import Content from './content';
import Video from './video';

const Feature = props => {
  const {
    root,
    fullContent,
    mainContent,
    titleContent,
    subTitleContent,
    textContent,
    btnContent,
    videoContent
  } = useStyle(defaultClasses, props.classes);

  const content = {
    title: 'Short',
    subTitle: 'Video',
    text:
      'Vídeos curtos virais, que estendem a aproximação e interação com a marca mesmo após a realização do Live Commerce',
    btnText: 'Assistir Mais'
  };
  return (
    <div className={root}>
      <div className={fullContent}>
        <Content
          content={content}
          classes={{
            mainContent,
            titleContent,
            subTitleContent,
            textContent,
            btnContent
          }}
        />
        <Video classes={{ root: videoContent }} />
      </div>
    </div>
  );
};

Feature.propTypes = {
  classes: shape({ root: string })
};
Feature.defaultProps = {};
export default Feature;
