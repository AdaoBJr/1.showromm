import React from 'react';
import { useStyle } from '@magento/venia-ui/lib/classify';
import { shape, string } from 'prop-types';
import { Play } from 'react-feather';
import Icon from '@magento/venia-ui/lib/components/Icon';

import defaultClasses from './video.css';

const Video = props => {
  const classes = useStyle(defaultClasses, props.classes);
  return (
    <div className={classes.root}>
      <div className={classes.iconContent}>
        <Icon src={Play} size={16} className={classes.iconPlay} />
      </div>
    </div>
  );
};

Video.propTypes = {
  classes: shape({ root: string })
};
Video.defaultProps = {};
export default Video;
