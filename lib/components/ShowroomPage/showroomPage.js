import React from 'react';
import { mergeClasses } from '@magento/venia-ui/lib/classify';
import { shape, string } from 'prop-types';

import defaultClasses from './showroomPage.css';
import Intro from './intro';
import Brands from './brands';
import Gallery from './gallery';
import Feature from './feature';
import Comments from './comments';
import Partner from './partner';

const ShowroomPage = props => {
  const classes = mergeClasses(defaultClasses, props.classes);
  return (
    <div className={classes.root}>
      <Intro />
      <Brands />
      <Gallery />
      <Feature />
      <Comments />
      <Partner />
    </div>
  );
};

ShowroomPage.propTypes = {
  classes: shape({ root: string })
};
ShowroomPage.defaultProps = {};
export default ShowroomPage;
