import React from 'react';
import { useStyle } from '@magento/venia-ui/lib/classify';
import { shape, string } from 'prop-types';

import defaultClasses from './partner.css';
import Button from '@magento/venia-ui/lib/components/Button';

const Partner = props => {
  const classes = useStyle(defaultClasses, props.classes);
  return (
    <div className={classes.root}>
      <div className={classes.contactForm}>
        <form className={classes.dataForm}>
          <h2 className={classes.titleForm}>Seja um parceiro do OQVestir</h2>
          <input type="text" placeholder="Nome" />
          <input type="text" placeholder="E-mail" className={classes.emailForm} />
          <textarea type="text" placeholder="Mensagem" />
          <Button
            type="submit"
            priority="high"
            classes={{ root_highPriority: classes.btnForm }}
          >
            Enviar
          </Button>
        </form>
      </div>
    </div>
  );
};

Partner.propTypes = {
  classes: shape({ root: string })
};
Partner.defaultProps = {};
export default Partner;
