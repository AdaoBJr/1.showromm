import React from 'react';
import { useStyle } from '@magento/venia-ui/lib/classify';
import { shape, string } from 'prop-types';
import Button from '@magento/venia-ui/lib/components/Button';

import defaultClasses from './content.css';

const Content = props => {
  const { content } = props;
  const classes = useStyle(defaultClasses, props.classes);
  return (
    <section className={classes.root}>
      <div className={classes.mainContent}>
        <h1 className={classes.titleContent}>{content.title}</h1>
        <h3 className={classes.subTitleContent}>{content.subTitle}</h3>
        <p className={classes.textContent}>{content.text}</p>
        <Button
          type="button"
          priority="high"
          classes={{ root_highPriority: classes.btnContent }}
        >
          {content.btnText}
        </Button>
      </div>
    </section>
  );
};
Content.propTypes = {
  classes: shape({ root: string })
};
Content.defaultProps = {};
export default Content;
