import React from 'react';
import { useStyle } from '@magento/venia-ui/lib/classify';
import { shape, string } from 'prop-types';
import Image from '@magento/venia-ui/lib/components/Image';
import Carousel from '@mcxbr/carousel';

import defaultClasses from './brands.css';
import img1 from '../../../docs/assets/mockBrands/oqvestir.png';
import img2 from '../../../docs/assets/mockBrands/dress_to.png';
import img3 from '../../../docs/assets/mockBrands/dzarm.png';
import img6 from '../../../docs/assets/mockBrands/vogueExame.png';

const Brands = props => {
  const classes = useStyle(defaultClasses, props.classes);
  const images = [img1, img2, img3, img1, img2, img3];

  const settings = {
    dots: false,
    autoplay: true,
    autoplaySpeed: 10000,
    arrows: false,
    speed: 10000,
    cssEase: 'linear',
    touchMove: false,
    centerMode: true
  };

  return (
    <div className={classes.root}>
      <div className={classes.topBrands}>
        <Carousel settings={settings}>
          {images.map((img, index) => (
            <div key={index}>
              <Image src={img} width="155px" />
            </div>
          ))}
        </Carousel>
      </div>
      <div className={classes.midBrands}>
        <img src={img6} />
      </div>
    </div>
  );
};

Brands.propTypes = {
  classes: shape({ root: string })
};
Brands.defaultProps = {};
export default Brands;
