import React from 'react';
import { useStyle } from '@magento/venia-ui/lib/classify';
import { shape, string } from 'prop-types';

import defaultClasses from './comments.css';
import Image from '@magento/venia-ui/lib/components/Image';
import Carousel from '@mcxbr/carousel';
import { DollarSign, BarChart, Activity } from 'react-feather';
import Icon from '@magento/venia-ui/lib/components/Icon';
import ca from '../../../docs/assets/mockComments/ca.png';
import dzarm from '../../../docs/assets/mockComments/dzarm.png';
import ida from '../../../docs/assets/mockComments/ida.png';

const Comments = props => {
  const {
    root,
    infoComments,
    dataComments,
    consumerComments,
    iconComments,
    commentContent,
    commentImage
  } = useStyle(defaultClasses, props.classes);

  const comments = [ca, dzarm, ida, ca, dzarm, ida];

  const contentComments = {
    text1: 'US$ 11 bi, é a estimativa para o mercado de live commerce em 2021 nos EUA.',
    text2: '66% de aumento de consumo de live streaming em 2020 no mundo.',
    text3: '45% dos chineses assistem a lives de produtos de 3 a 5 vezes por semana.'
  };

  return (
    <div className={root}>
      <Carousel>
        {comments.map((commentImg, index) => (
          <div key={index}>
            <Image src={commentImg} width="780px" />
          </div>
        ))}
      </Carousel>
      <div className={infoComments}>
        <div className={dataComments}>
          <div>
            <Icon src={DollarSign} size={33} className={iconComments} />
            <p>{contentComments.text1}</p>
          </div>
          <div className={consumerComments}>
            <Icon src={BarChart} size={33} className={iconComments} />
            <p>{contentComments.text2}</p>
          </div>
          <div>
            <Icon src={Activity} size={33} className={iconComments} />
            <p>{contentComments.text3}</p>
          </div>
        </div>
      </div>
    </div>
  );
};

Comments.propTypes = {
  classes: shape({ root: string })
};
Comments.defaultProps = {};
export default Comments;
