import React from 'react';
import { useStyle } from '@magento/venia-ui/lib/classify';
import { shape, string } from 'prop-types';

import defaultClasses from './gallery.css';
import imgVideo from '../../../docs/assets/mockBrands/img_video2.png';
import Carousel from '@mcxbr/carousel';
import Content from './content';
import Video from './video';

const Gallery = props => {
  const {
    root,
    fullContent,
    mainContent,
    titleContent,
    subTitleContent,
    textContent,
    btnContent,
    videoContent,
    infoLive,
    titleLive,
    dataLive,
    peopleInLive,
    fittingRoom,
    titleFittingRoom,
    videoContainer,
    videoBrands
  } = useStyle(defaultClasses, props.classes);

  const content = {
    title: 'Provador Live:',
    subTitle: 'sua marca ao vivo para mais de 100 mil pessoas com apenas um click.',
    text:
      'Seguindo a tendência do Live Commerce que já é sucesso absoluto na China, o Provador Live entrega entretenimento, aprendizado e conversão de vendas, apresentado por uma comunicadora com relação muito próxima com o público.',
    btnText: 'Minha marca no OqVestir'
  };

  const contentLive = {
    title: 'Todas as terças e domingos,',
    subTitle: 'às 20:00, no Instagram @OQVestir',
    subTitle1: {
      title: '+ 100000',
      subTitle: 'de impacto de pessoas por live.'
    },
    subTitle2: {
      title: '+ 3500',
      subTitle: 'pessoas simultâneas na live.'
    },
    subTitle3: {
      title: 'Peças esgotadas',
      subTitle: 'durante a Live.'
    }
  };

  const videos = [imgVideo, imgVideo, imgVideo, imgVideo, imgVideo];

  const settings = {
    centerMode: true
  };

  return (
    <div className={root}>
      <div className={fullContent}>
        <Video classes={{ root: videoContent }} />
        <Content
          content={content}
          classes={{
            mainContent,
            titleContent,
            subTitleContent,
            textContent,
            btnContent
          }}
        />
      </div>
      <div className={infoLive}>
        <div className={titleLive}>
          <h2>{contentLive.title}</h2>
          <p>{contentLive.subTitle}</p>
        </div>
        <div className={dataLive}>
          <div>
            <h2>{contentLive.subTitle1.title}</h2>
            <p>{contentLive.subTitle1.subTitle}</p>
          </div>
          <div className={peopleInLive}>
            <h2>{contentLive.subTitle2.title}</h2>
            <p>{contentLive.subTitle2.subTitle}</p>
          </div>
          <div>
            <h2>{contentLive.subTitle3.title}</h2>
            <p>{contentLive.subTitle3.subTitle}</p>
          </div>
        </div>
      </div>
      <div className={fittingRoom}>
        <h2 className={titleFittingRoom}>Assista aqui o Provador Live</h2>
        <Carousel settings={settings}>
          {videos.map((_, index) => (
            <div key={index} classes={videoContainer}>
              <Video classes={{ root: videoBrands }} />
            </div>
          ))}
        </Carousel>
      </div>
    </div>
  );
};

Gallery.propTypes = {
  classes: shape({ root: string })
};
Gallery.defaultProps = {};
export default Gallery;
