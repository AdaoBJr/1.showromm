import React from 'react';
import { useStyle } from '@magento/venia-ui/lib/classify';
import { shape, string } from 'prop-types';

import defaultClasses from './intro.css';
import Video from './video';
import Content from './content';

const Intro = props => {
  const {
    root,
    mainContent,
    titleContent,
    subTitleContent,
    textContent,
    btnContent,
    videoContent
  } = useStyle(defaultClasses, props.classes);

  const content = {
    title: 'Showroom OqVestir',
    subTitle: 'Venha fazer parte do maior Showroom de moda premium do Brasil.',
    text:
      'Com uma comunicação única, a influenciadora gera alto índice de vendas e envolvimento dos consumidores com as marcas expostas na live',
    btnText: 'Minha marca no OqVestir'
  };

  return (
    <section className={root}>
      <Content
        content={content}
        classes={{ mainContent, titleContent, subTitleContent, textContent, btnContent }}
      />
      <Video classes={{ root: videoContent }} />
    </section>
  );
};

Intro.propTypes = {
  classes: shape({ root: string })
};
Intro.defaultProps = {};
export default Intro;
