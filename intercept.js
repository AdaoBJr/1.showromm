module.exports = targets => {
  targets.of('@magento/pwa-buildpack').specialFeatures.tap(flags => {
    flags[targets.name] = {
      esModules: true,
      cssModules: true,
      i18n: true
    };
  });

  targets.of('@magento/venia-ui').routes.tap(routes => {
    return [
      ...routes,
      {
        name: 'Showroom',
        pattern: '/showroom',
        path: '@mcxbr/showroom/lib/components/ShowroomPage',
        exact: true
      },
    ];
  });
};
